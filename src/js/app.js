import * as functions from './modules/functions.js';
import Swiper, { Navigation, Pagination, HashNavigation, Keyboard } from 'swiper';

functions.isWebp();

const teamSlider = new Swiper('.team__slider', {
	modules: [Navigation, Pagination, HashNavigation, Keyboard],
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: true,
	},
	grabCursor: true,
	hashNavigation: true,
	keyboard: true
})


const app = Vue.createApp({
	// components: {

	// }
	data() {
		return {
			documentWidth: document.documentElement.clientWidth,
			headerMenuVisible: false,
			mpTools: {
				cards: [
					{
						name: 'Figma',
						status: 'Free',
						img: 'figma',
						like: false
					},
					{
						name: 'Sketch',
						status: 'Trial & Paid',
						img: 'sketch',
						like: false
					},
					{
						name: 'Notion',
						status: 'Free & Paid',
						img: 'notion',
						like: false
					},
					{
						name: 'Slack',
						status: 'Free & Paid',
						img: 'slack',
						like: false
					},
					{
						name: 'Invision',
						status: 'Free & Paid',
						img: 'invision',
						like: false
					},
					{
						name: 'Sketch',
						status: 'Free & Paid',
						img: 'sketch',
						like: false
					},
					{
						name: 'Visual Studio Code',
						status: 'Free',
						img: 'visual-studio-code',
						like: false
					},
					{
						name: 'Figma',
						status: 'Free',
						img: 'figma',
						like: false
					},
					{
						name: 'Notion',
						status: 'Free & Paid',
						img: 'notion',
						like: false
					},
					{
						name: 'Slack',
						status: 'Free & Paid',
						img: 'slack',
						like: false
					},
					{
						name: 'Invision',
						status: 'Free & Paid',
						img: 'invision',
						like: false
					}
				],
				cardNum: 6,
				defaultCardNum: 6
			},
			newcomerTools: {
				cards: [
					{
						name: 'Zeplin',
						status: 'Free & Paid',
						img: 'zeplin',
						like: false
					},
					{
						name: 'PHPStorm',
						status: 'Free',
						img: 'phpstorm',
						like: false
					},
					{
						name: 'Toolbox',
						status: 'Free',
						img: 'toolbox',
						like: false
					},
					{
						name: 'Procreate',
						status: 'Paid',
						img: 'procreate',
						like: false
					}
				],
				cardNum: 4,
				defaultCardNum: 4
			}
		}
	},
	beforeMount() {
		if (this.documentWidth <= 479) {
			this.mpTools.defaultCardNum = 2;
			this.newcomerTools.defaultCardNum = 2;
		} else if (this.documentWidth <= 479) {
			this.mpTools.defaultCardNum = 4;
		}
		this.mpTools.cardNum = this.mpTools.defaultCardNum;
		this.newcomerTools.cardNum = this.newcomerTools.defaultCardNum;
	},
	watch: {
		mpToolsCardNum(newNum, oldNum) {
			if (newQuestion.indexOf('?') > -1) {
				this.getAnswer()
			}
		}
	},
	methods: {
	}
}).mount('#app')

app.use(Swiper, {
});
