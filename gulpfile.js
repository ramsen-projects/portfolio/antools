import gulp from 'gulp';
import { path } from './gulp/config/path.js';
import { plugins } from './gulp/config/plugins.js';

global.app = {
	isBuild: process.argv.includes('--build'),
	isDev: !process.argv.includes('--build'),
	gulp: gulp,
	path: path,
	plugins: plugins
}

import { reset } from './gulp/tasks/reset.js';
import { copy } from './gulp/tasks/copy.js';
import { server } from './gulp/tasks/server.js';
import { html } from './gulp/tasks/html.js';
import { scss } from './gulp/tasks/scss.js';
import { js } from './gulp/tasks/js.js';
import { img } from './gulp/tasks/img.js';
import { otfToTtf, ttfToWoff, fontsStyle } from './gulp/tasks/fonts.js';
import { svgSprite } from './gulp/tasks/svgSprite.js';
import { zip } from './gulp/tasks/zip.js';
import { ftp } from './gulp/tasks/ftp.js';

function watcher() {
	gulp.watch(path.watch.assets, gulp.series(reset, copy));
	gulp.watch(path.watch.html, html);
	gulp.watch(path.watch.scss, scss);
	gulp.watch(path.watch.js, js);
	gulp.watch(path.watch.img, img);
}

const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle);
const main = gulp.series(fonts, gulp.parallel(copy, html, scss, js, img));

const dev = gulp.series(reset, main, gulp.parallel(watcher, server));
const build = gulp.series(reset, main);
const deployZIP = gulp.series(reset, main, zip);
const deployFTP = gulp.series(reset, main, ftp);

export { svgSprite }
export { dev }
export { build }
export { deployZIP }
export { deployFTP }

gulp.task('default', dev);