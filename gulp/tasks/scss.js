import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import rename from 'gulp-rename';
import cleanCss from 'gulp-clean-css';
import webpCss from 'gulp-webpcss';
import autoPrefixer from 'gulp-autoprefixer';
import groupCssMediaQueires from 'gulp-group-css-media-queries';

const sass = gulpSass(dartSass);

export const scss = () => {
	return app.gulp.src(app.path.src.scss, { sourcemaps: app.isDev })
		.pipe(app.plugins.plumber(
			app.plugins.notify.onError({
				title: "SCSS",
				message: "Error <%= error.message%>"
			})
		))
		.pipe(app.plugins.replace(/@img\//g, '../img/'))
		.pipe(sass({
			includePaths: ['node_modules'],
			outputStyle: app.isBuild ? 'compressed' : 'expanded'
		}))
		.pipe(app.plugins.ifPlugin(
			app.isBuild,
			groupCssMediaQueires()
		))
		.pipe(app.plugins.ifPlugin(
			app.isBuild,
			webpCss({
				webpClass: ".webp",
				noWebpClass: ".no-webp"
			})
		))
		.pipe(app.plugins.ifPlugin(
			app.isBuild,
			autoPrefixer({
				grid: true,
				overrideBrowserslist: ["last 3 versions"],
				cascade: true
			})
		))
		.pipe(app.gulp.dest(app.path.build.css))
		.pipe(app.plugins.ifPlugin(
			app.isBuild,
			cleanCss()
		))
		.pipe(rename({
			extname: ".min.css"
		}))
		.pipe(app.gulp.dest(app.path.build.css))
		.pipe(app.plugins.browserSync.stream())
}