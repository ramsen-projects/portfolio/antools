import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = `./dist`;
const srcFolder = `./src`;

export const path = {
	build: {
		html: `${buildFolder}/`,
		css: `${buildFolder}/css/`,
		js: `${buildFolder}/js/`,
		img: `${buildFolder}/img/`,
		fonts: `${buildFolder}/fonts/`,
		assets: `${buildFolder}/assets/`
	},
	src: {
		html: `${srcFolder}/*.html`,
		scss: `${srcFolder}/scss/style.scss`,
		js: `${srcFolder}/js/app.js`,
		img: `${srcFolder}/img/**/*.{jpg,jpeg,gif,png,webp}`,
		svg: `${srcFolder}/img/**/*.svg`,
		svgicons: `${srcFolder}/svgicons/*.svg`,
		assets: `${srcFolder}/assets/**/*.*`
	},
	watch: {
		html: `${srcFolder}/**/*.html`,
		scss: `${srcFolder}/**/*.scss`,
		js: `${srcFolder}/**/*.js`,
		img: `${srcFolder}/img/**/*.{jpg,jpeg,gif,png,webp,ico,svg}`,
		assets: `${srcFolder}/assets/**/*.*`
	},
	clean: buildFolder,
	buildFolder: buildFolder,
	srcFolder: srcFolder,
	rootFolder: rootFolder,
	ftp: `test`
}